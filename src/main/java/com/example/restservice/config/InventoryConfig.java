package com.example.restservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConstructorBinding
@ConfigurationProperties(prefix = "inventory")
public record InventoryConfig(String baseUrl, String apiKey) { }
