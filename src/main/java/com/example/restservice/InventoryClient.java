package com.example.restservice;

import com.example.restservice.config.InventoryConfig;
import com.example.restservice.models.AnimalInventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class InventoryClient {
    private final WebClient webClient;

    @Autowired
    public InventoryClient(InventoryConfig config) {
        this.webClient = WebClient.builder()
                .baseUrl(config.baseUrl())
                .defaultHeader("X-Api-Key", config.apiKey())
                .build();
    }

    public Mono<AnimalInventory> getInventory() {
        return this.webClient.get().uri("/dev/animals").retrieve().bodyToMono(AnimalInventory.class);
    }
}
