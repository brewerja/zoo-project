package com.example.restservice;

import com.example.restservice.models.Animal;
import com.example.restservice.models.Exhibit;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ExhibitCreator {

    public List<Exhibit> createExhibits(List<Animal> animals) {
        List<Exhibit> exhibits = new ArrayList<>();
        Map<String, List<Animal>> speciesGroups = getSpeciesGroups(animals);
        for (Map.Entry<String, List<Animal>> entry : speciesGroups.entrySet()) {
            exhibits.addAll(createAllExhibitsOfASpecies(entry.getValue()));
        }
        return exhibits;
    }

    private Map<String, List<Animal>> getSpeciesGroups(List<Animal> animals) {
        Map<String, List<Animal>> speciesGroups = new HashMap<>();
        for (Animal animal : animals) {
            if (!speciesGroups.containsKey(animal.species())) {
                speciesGroups.put(animal.species(), new ArrayList<>());
            }
            speciesGroups.get(animal.species()).add(animal);
        }
        return speciesGroups;
    }

    public List<Exhibit> createAllExhibitsOfASpecies(List<Animal> animalsOfSpecies) {
        List<Exhibit> exhibitsOfSpecies = new ArrayList<>();
        // sorted largest to smallest
        animalsOfSpecies.sort(Comparator.comparing(Animal::spaces).reversed());

        String species = animalsOfSpecies.get(0).species();
        Exhibit exhibit = new Exhibit(species);
        while (animalsOfSpecies.size() > 0) {
            int capacity = exhibit.getRemainingCapacity();
            Animal animalToAdd = null;
            for (int i = 0; i < animalsOfSpecies.size(); ++i) {
                if (animalsOfSpecies.get(i).spaces() <= capacity) {
                    animalToAdd = animalsOfSpecies.remove(i);
                    break;
                }
            }
            if (animalToAdd != null) {
                exhibit.addAnimal(animalToAdd);
            } else {
                // either animals too big for remaining capacity or exhibit is full
                exhibitsOfSpecies.add(exhibit);
                exhibit = new Exhibit(species);
            }
        }
        if (exhibit.getAnimals().size() > 0) {
            // don't add an empty exhibit
            exhibitsOfSpecies.add(exhibit);
        }
        return exhibitsOfSpecies;
    }

}
