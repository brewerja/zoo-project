package com.example.restservice;

import com.example.restservice.models.AnimalInventory;
import com.example.restservice.models.Exhibit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ExhibitsController {

    @Autowired
    InventoryClient inventoryClient;

    @Autowired
    ExhibitCreator exhibitCreator;

    @GetMapping("/exhibits")
    public List<Exhibit> exhibits() {
        AnimalInventory inventory = inventoryClient.getInventory().block();
        return exhibitCreator.createExhibits(inventory.animals());
    }
}
