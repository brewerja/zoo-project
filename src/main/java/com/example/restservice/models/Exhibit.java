package com.example.restservice.models;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class Exhibit {

    private static final int DEFAULT_SPACES = 20;
    int spaces;
    List<Animal> animals;
    String species;

    public Exhibit(String species) {
        this(DEFAULT_SPACES, species);
    }
    public Exhibit(int spaces, String species) {
        this.species = species;
        this.spaces = spaces;
        this.animals = new ArrayList<>();
    }

    public void addAnimal(Animal animal) {
        this.animals.add(animal);
    }

    public int getRemainingCapacity() {
        return this.spaces - this.animals.stream().map(Animal::spaces).reduce(0, Integer::sum);
    }
}
