package com.example.restservice.models;

public record Animal(String species, int spaces) { }
