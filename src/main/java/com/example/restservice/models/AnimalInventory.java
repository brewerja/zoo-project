package com.example.restservice.models;

import java.util.List;

public record AnimalInventory(List<Animal> animals) {}
