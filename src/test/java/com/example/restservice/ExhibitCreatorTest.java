package com.example.restservice;

import com.example.restservice.config.InventoryConfig;
import com.example.restservice.models.Animal;
import com.example.restservice.models.AnimalInventory;
import com.example.restservice.models.Exhibit;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ExhibitCreatorTest {

    @Autowired
    private InventoryClient client;

    @Test
    void testClient() {
        AnimalInventory inventory = client.getInventory().block();
        ExhibitCreator creator = new ExhibitCreator();
        List<Exhibit> exhibits = creator.createExhibits(inventory.animals());
        assertTrue(inventory.animals().size() > 0);
        assertTrue(exhibits.size() > 0);
    }

    @Test
    void createExhibits() {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("lion", 5));
        animals.add(new Animal("lion", 10));
        animals.add(new Animal("tiger", 19));
        animals.add(new Animal("tiger", 15));
        animals.add(new Animal("tiger", 5));

        List<Exhibit> exhibits = new ExhibitCreator().createExhibits(animals);

        assertEquals("tiger", exhibits.get(0).getAnimals().get(0).species());
        assertEquals(1, exhibits.get(0).getAnimals().size());
        assertEquals("tiger", exhibits.get(1).getAnimals().get(0).species());
        assertEquals(2, exhibits.get(1).getAnimals().size());
        assertEquals("lion", exhibits.get(2).getAnimals().get(0).species());
        assertEquals(3, exhibits.size());
        assertEquals(2, exhibits.get(2).getAnimals().size());
    }

    @Test
    void createAllExhibitsOfASpecies() {
        ExhibitCreator creator = new ExhibitCreator();
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("lion", 5));
        animals.add(new Animal("lion", 10));
        animals.add(new Animal("lion", 19));
        animals.add(new Animal("lion", 15));
        animals.add(new Animal("lion", 5));
        List<Exhibit> exhibits = creator.createAllExhibitsOfASpecies(animals);
        assertEquals(3, exhibits.size());
    }
}